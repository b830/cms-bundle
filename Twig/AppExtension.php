<?php

namespace App\Akip\CmsBundle\Twig;

use App\Akip\CmsBundle\Entity\Section;
use App\Akip\CmsBundle\Entity\Setting;
use App\Akip\EshopBundle\Entity\Category;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Symfony\Component\Filesystem\Filesystem;


class AppExtension extends AbstractExtension
{
    /**
     * @var EntityManagerInterface|ManagerRegistry
     */
    protected $em;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * AppExtension constructor.
     * @param ManagerRegistry $em
     * @param ContainerInterface $container
     * @param Filesystem $filesystem
     */
    public function __construct(ManagerRegistry $em, ContainerInterface $container, Filesystem $filesystem)
    {
        $this->em = $em;
        $this->container = $container;
        $this->filesystem = $filesystem;
    }

    public function getFunctions()
    {
        return [
//            new TwigFunction('cms_menu_items', [$this, 'getMenuItems'])
            new TwigFunction('getMainCategories', [$this, 'getMainCategories']),
            new TwigFunction('getCategoryBySlugAndDepth', [$this, 'getCategoryBySlugAndDepth']),
            new TwigFunction('getSectionBySlug', [$this, 'getSectionBySlug']),
            new TwigFunction('getSettingBySlug', [$this, 'getSettingBySlug']),
            new TwigFunction('getSettingByGroup', [$this, 'getSettingByGroup']),
            new TwigFunction('createImgPath', [$this, 'createImgPath']),
            new TwigFunction('strpad', [$this, 'strpad']),
        ];
    }

    public function getFilters()
    {
        return array(
            'json_decode' => new TwigFilter('json_decode', [$this, 'jsonDecode']),
            'strpad' => new TwigFilter('strpad', [$this, 'strpad']),
        );
    }

    public function jsonDecode($str)
    {
        return json_decode($str);
    }


    public function getMainCategories()
    {

        $categories = $this->em->getRepository(Category::class)->findBy(['parent' => null, 'enabled' => true], ['sort' => 'ASC']);
//        $categories = $this->em->getRepository(Category::class)->findMain();
        return $categories;
    }

    public function getCategoryBySlugAndDepth($slug, $depth)
    {
        $categories = [];
        if (is_array($slug)) {
            foreach ($slug as $item) {
                $categories[] = $this->em->getRepository(Category::class)->findBySlugAndDepth($item, $depth);
            }
        } else {
            $categories = $this->em->getRepository(Category::class)->findBySlugAndDepth($slug, $depth);
        }
        return $categories;
    }

    public function getSectionBySlug($slug)
    {
        $section = $this->em->getRepository(Section::class)->findBySlug($slug);
        return $section;
    }

    public function getSettingBySlug($slug)
    {
        $setting = $this->em->getRepository(Setting::class)->findOneBy(['slug' => $slug]);
        return $setting;
    }

    public function getSettingByGroup($group)
    {
        $settings = $this->em->getRepository(Setting::class)->findBy(['group' => $group]);
        $items = array();
        foreach ($settings as $setting) {
            if (!$setting->getIsHtml())
                $setting->setValue(strip_tags($setting->getValue()));

            $items[$setting->getSlug()] = $setting;
        }
        return $items;
    }

    //pokud existuje složka, kde 1. tři znaky se shodují s 1. třemi znaky souboru, tak se vyhledávaný soubor nacházý v této složce
    public function checkFolderName(string $folderName, string $fileName): bool
    {
        if (substr($folderName, 0, 3) === substr($fileName, 0, 3)) {
            return true;
        } else {
            return false;
        }
    }

    //získáme array, který obsahuje cestu k webp a verzi png obrázku
    public function createImgPath($parentUuid, $uuid = null)
    {
        $path = $this->container->getParameter('kernel.project_dir') . '/public/files';
        if ($parentUuid === null) {
            return [
                'webp' => '/img/File_not_found.webp',
                'img' =>  '/img/File_not_found.png'
            ];
        }
        if ($uuid == null) {
            $uuid = $parentUuid;
        }
        if (!file_exists($path)) {
            return [
                'webp' =>  '/img/File_not_found.webp',
                'img' =>  '/img/File_not_found.png'
            ];
        }
        $filePath = [
            'webp' => glob("{$path}/{$parentUuid}/{$uuid}.webp"),
            'img' => glob("{$path}/{$parentUuid}/{$uuid}.[!w]*"),
        ];
        if (!empty($filePath['webp']) and !empty($filePath['img'])) {
            return [
                'webp' => '/files' . str_replace($path,'',$filePath['webp'][0]),
                'img' => '/files' . str_replace($path,'',$filePath['img'][0])
            ];
        } else {
            $finder = new Finder();
            foreach ($finder->directories()->in($path) as $item) {
                if ($this->checkFolderName($item->getFilename(), $parentUuid)) {
                    $filePath = [
                        'webp' => glob("{$item->getFilename()}/{$uuid}.webp"),
                        'img' => glob("{$item->getFilename()}/{$uuid}.[!w]*"),
                    ];
                }
                if (!empty($filePath['webp']) and !empty($filePath['img'])) {
//                    return ($filePath);
                    return [
                        'webp' => '/files' . str_replace($path,'',$filePath['webp'][0]),
                        'img' => '/files' . str_replace($path,'',$filePath['img'][0])
                    ];
                } else {
                    return [
                        'webp' => '/img/File_not_found.webp',
                        'img' => '/img/File_not_found.png'
                    ];
                }
            }
        }
    }

    public function strpad($number, $pad_length, $pad_string) {
        return str_pad($number, $pad_length, $pad_string, STR_PAD_LEFT);
    }
}
