<?php

namespace App\Akip\CmsBundle\Repository;

use App\Akip\CmsBundle\Entity\Menu;
use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Entity\MenuItemTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Menu|null find($id, $lockMode = null, $lockVersion = null)
 * @method Menu|null findOneBy(array $criteria, array $orderBy = null)
 * @method Menu[]    findAll()
 * @method Menu[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Menu::class);
    }

    public function build(Menu $menu, $menuItems, MenuItemRepository $menuItemRepository, MenuItemTranslationRepository $menuItemTranslationRepository, $depth = 0)
    {
        $result = array();
        $maxDpth = 0; //maximalni hloubka stromu
        foreach ($menuItems as $menuItem) {
            $depth_arr[] = $menuItem->getDepth();
            $maxDpth = max($depth_arr);
        }

        foreach ($menuItems as $menuItem) {
            if ($menuItem->getDepth() === 1) {
                #region getting Depth

                if ($menuItem->getDepth() + 1 <= $maxDpth)
                    $tmpChildren = $menu->getByDepth($menuItem->getDepth() + 1);
                else
                    $tmpChildren = [];


                if ($menuItem->getDepth() + 2 <= $maxDpth)
                    $tmpChildren2 = $menu->getByDepth($menuItem->getDepth() + 2);
                else
                    $tmpChildren2 = [];


                if ($menuItem->getDepth() + 3 <= $maxDpth)
                    $tmpChildren3 = $menu->getByDepth($menuItem->getDepth() + 3);
                else
                    $tmpChildren3 = [];


                if ($menuItem->getDepth() + 4 <= $maxDpth)
                    $tmpChildren4 = $menu->getByDepth($menuItem->getDepth() + 4);
                else
                    $tmpChildren4 = [];

                if ($menuItem->getDepth() + 5 <= $maxDpth)
                    $tmpChildren5 = $menu->getByDepth($menuItem->getDepth() + 5);
                else
                    $tmpChildren5 = [];

                if ($menuItem->getDepth() + 6 <= $maxDpth)
                    $tmpChildren6 = $menu->getByDepth($menuItem->getDepth() + 6);
                else
                    $tmpChildren6 = [];

                if ($menuItem->getDepth() + 7 <= $maxDpth)
                    $tmpChildren7 = $menu->getByDepth($menuItem->getDepth() + 7);
                else
                    $tmpChildren7 = [];
                #endregion

                $tmpMain = $this->setFields($menuItem, $menu, $menuItemTranslationRepository);

                foreach ($tmpChildren as $tmpChild) {
                    if ($menuItem === $tmpChild->getParent()) {
                        array_push($tmpMain['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                    }
                }

                for ($i = 0; $i < count($tmpMain['children']); $i++) {
                    foreach ($tmpChildren2 as $tmpChild) {
                        ;
                        if ($tmpMain['children'][$i]['id'] === $tmpChild->getParent()->getId()) {
                            array_push($tmpMain['children'][$i]['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                        }
                    }
                }

                for ($y = 0; $y < count($tmpMain['children']); $y++) {
                    for ($z = 0; $z < count($tmpMain['children'][$y]['children']); $z++) {
                        foreach ($tmpChildren3 as $tmpChild) {
                            if ($tmpMain['children'][$y]['children'][$z]['id'] === $tmpChild->getParent()->getId()) {
                                array_push($tmpMain['children'][$y]['children'][$z]['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                            }
                        }
                    }
                }

                for ($y = 0; $y < count($tmpMain['children']); $y++) {
                    for ($z = 0; $z < count($tmpMain['children'][$y]['children']); $z++) {
                        for ($x = 0; $x < count($tmpMain['children'][$y]['children'][$z]['children']); $x++) {
                            foreach ($tmpChildren4 as $tmpChild) {
                                if ($tmpMain['children'][$y]['children'][$z]['children'][$x]['id'] === $tmpChild->getParent()->getId()) {

                                    array_push($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                                }
                            }
                        }
                    }
                }

                for ($y = 0; $y < count($tmpMain['children']); $y++) {
                    for ($z = 0; $z < count($tmpMain['children'][$y]['children']); $z++) {
                        for ($x = 0; $x < count($tmpMain['children'][$y]['children'][$z]['children']); $x++) {
                            for ($t = 0; $t < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children']); $t++) {
                                foreach ($tmpChildren5 as $tmpChild) {
                                    if ($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['id'] === $tmpChild->getParent()->getId()) {
                                        array_push($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                                    }
                                }
                            }
                        }
                    }
                }

                for ($y = 0; $y < count($tmpMain['children']); $y++) {
                    for ($z = 0; $z < count($tmpMain['children'][$y]['children']); $z++) {
                        for ($x = 0; $x < count($tmpMain['children'][$y]['children'][$z]['children']); $x++) {
                            for ($t = 0; $t < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children']); $t++) {
                                for ($c = 0; $c < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children']); $c++) {
                                    foreach ($tmpChildren6 as $tmpChild) {
                                        if ($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['id'] === $tmpChild->getParent()->getId()) {
                                            array_push($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for ($y = 0; $y < count($tmpMain['children']); $y++) {
                    for ($z = 0; $z < count($tmpMain['children'][$y]['children']); $z++) {
                        for ($x = 0; $x < count($tmpMain['children'][$y]['children'][$z]['children']); $x++) {
                            for ($t = 0; $t < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children']); $t++) {
                                for ($c = 0; $c < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children']); $c++) {
                                    for ($v = 0; $v < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children']); $v++) {
                                        foreach ($tmpChildren6 as $tmpChild) {
                                            if ($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children'][$v]['id'] === $tmpChild->getParent()->getId()) {
                                                array_push($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children'][$v]['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for ($y = 0; $y < count($tmpMain['children']); $y++) {
                    for ($z = 0; $z < count($tmpMain['children'][$y]['children']); $z++) {
                        for ($x = 0; $x < count($tmpMain['children'][$y]['children'][$z]['children']); $x++) {
                            for ($t = 0; $t < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children']); $t++) {
                                for ($c = 0; $c < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children']); $c++) {
                                    for ($v = 0; $v < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children']); $v++) {
                                        for ($n = 0; $n < count($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children'][$v]['children']); $n++) {
                                            foreach ($tmpChildren7 as $tmpChild) {
                                                if ($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children'][$v]['children'][$n]['id'] === $tmpChild->getParent()->getId()) {
                                                    array_push($tmpMain['children'][$y]['children'][$z]['children'][$x]['children'][$t]['children'][$c]['children'][$v]['children'][$n]['children'], $this->setFields($tmpChild, $menu, $menuItemTranslationRepository));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                array_push($result, $tmpMain);
            }
        }
        return $result;
    }


    private function setFields(MenuItem $menuItem, Menu $menu, MenuItemTranslationRepository $menuItemTranslationRepository, $child = [])
    {
        $items = array();
        if (!$menuItem->getMenuItemTranslations()->isEmpty()) {
            foreach ($menuItem->getMenuItemTranslations() as $translation) {
                $items[$translation->getLocale()] = $translation;
            }
        }
        $parent = null;
        if ($menuItem->getParent())
            $parent = $menuItem->getParent()->getId();
        return [
            'id' => $menuItem->getId(),
            'name' => $menuItem->getName(),
            'lft' => $menuItem->getLft(),
            'rft' => $menuItem->getRgt(),
            'sort' => $menuItem->getSort(),
            'depth' => $menuItem->getDepth(),
            'route' => $menuItem->getRoute(),
            'class' => $menuItem->getClass(),
            'route_parameters' => $menuItem->getRouteParameters(),
            'translations' => $items,
            'parent' => $parent,
            'children' => $child
        ];
    }
    // /**
    //  * @return Menu[] Returns an array of Menu objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Menu
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public static function searchedColumns(): array
    {
        return [
            'id',
            'name',
            'slug'
        ];
    }
}
