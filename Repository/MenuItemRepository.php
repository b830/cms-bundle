<?php

namespace App\Akip\CmsBundle\Repository;

use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Entity\MenuItemTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MenuItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method MenuItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method MenuItem[]    findAll()
 * @method MenuItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MenuItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuItem::class);
    }

//    public function findByMenu($menuName, $locale)
//    {
//        return $this->createQueryBuilder('mi')
//            ->andWhere('mi.locale = :locale')
//            ->andWhere('mi.menu = = :menuName')
//            ->setParameter('locale', $locale)
//            ->setParameter('menuName', $menuName)
//            ->orderBy('mi.sort', 'ASC')
//            ->getQuery()
//            ->getResult();
//    }

    /**
     * Metodu je potreba zavolat po kazde uprave menu (pridani/uprava/smazani polozky, zmena razeni, zmena rodice apod.)
     * @see https://php.vrana.cz/traverzovani-kolem-stromu-prakticky.php
     * @see https://www.interval.cz/clanky/metody-ukladani-stromovych-dat-v-relacnich-databazich/
     *
     * @param $menuId
     * @param null $parentId
     * @param int $left
     * @return int|mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function rebuildTree($menuId, $parentId = null, $left = 0, $depth = 0)
    {
        $right = $left + 1;

        $items = $this->findBy(['menu' => $menuId, 'parent' => $parentId], ['sort' => 'ASC']);

        /**
         * @var $item MenuItem
         */
        foreach ($items as $item) {
            $right = $this->rebuildTree($menuId, $item->getId(), $right, $depth + 1);
        }

        if ($parentId) {
            $item = $this->find($parentId);
            $item->setLft($left);
            $item->setRgt($right);
            $item->setDepth($depth);

            $em = $this->getEntityManager();
            $em->persist($item);
            $em->flush();
        }

        return $right + 1;
    }

    public function getMenuItems($menuId, $locale)
    {
        $qb = $this->createQueryBuilder('mi')
            ->select('mi', 'p', 'mit')
            ->leftJoin(MenuItem::class, 'p', Expr\Join::WITH, 'mi.parent = p.id')
            ->leftJoin(MenuItemTranslation::class, 'mit', Expr\Join::WITH, 'mi.id = mit.menuItem')
            ->andWhere('mi.menu = :menu')
            ->andWhere('mit.locale = :locale OR mit.locale IS NULL')
            ->setParameter('menu', $menuId)
            ->setParameter('locale', $locale)
            ->orderBy('mi.lft', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_SCALAR);

        return $qb;
    }

    // /**
    //  * @return MenuItem[] Returns an array of MenuItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MenuItem
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
