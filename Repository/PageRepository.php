<?php

namespace App\Akip\CmsBundle\Repository;

use App\Akip\CmsBundle\Entity\Page;
use App\Akip\CmsBundle\Entity\PageTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, array $orderBy = null)
 * @method Page[]    findAll()
 * @method Page[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PageRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public static function searchedColumns(): array
    {
        return [
            'id',
            'title'
//            'createdAt'
        ];
    }

    public function findArticle($id, $locale)
    {
        try {
            $article = $this->createQueryBuilder('a')
                ->join(PageTranslation::class, 'at')
                ->andWhere('a.id = :id')
                ->setParameter('id', $id)
                ->andWhere('at.locale = :locale')
                ->setParameter('locale', $locale)
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }

        // najit spravny preklad
        // projdeme vsechny preklady a najdeme ten, kde souhlasi locale
        $article->translation = $article->getTranslationsObj()->filter(function (PageTranslation $at) use ($locale) {
            return $at->getLocale() === $locale;
        })->first();

        return $article;
    }
    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

}
