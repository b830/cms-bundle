<?php


namespace App\Akip\CmsBundle\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\CompositeExpression;

abstract class BaseEntityRepository extends ServiceEntityRepository
{

    public static abstract function searchedColumns(): array;

    public function getSearchCriteria($value): CompositeExpression
    {
        $exp = new CompositeExpression(CompositeExpression::TYPE_OR,
            array_map(function ($field) use ($value) {
                return Criteria::expr()->contains($field, $value);
            }, $this::searchedColumns())
        );

        return $exp;
    }


}