<?php

namespace App\Akip\CmsBundle\Repository;

use App\Akip\CmsBundle\Entity\Section;
use App\Akip\CmsBundle\Entity\Setting;
use App\Akip\CmsBundle\Entity\Slider;
use App\Akip\CmsBundle\Entity\SliderSlide;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SliderSlide|null find($id, $lockMode = null, $lockVersion = null)
 * @method SliderSlide|null findOneBy(array $criteria, array $orderBy = null)
 * @method SliderSlide[]    findAll()
 * @method SliderSlide[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SliderSlideRepository extends BaseEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SliderSlide::class);
    }

    // /**
    //  * @return Section[] Returns an array of Section objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Section
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public static function searchedColumns(): array
    {
        return [
            'name',
            'id',
        ];
    }
}
