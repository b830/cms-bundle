<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Controller\api\BaseController;
use App\Akip\EshopBundle\Entity\Order;
use App\Akip\EshopBundle\Repository\OrderRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class DashboardController
 * @package App\Akip\CmsBundle\Controller
 * @Rest\Route("/api/route/", name="route_")
 */
class RouteController extends BaseController
{
    private $em;

    const PAGE = 'cms_page';
    const NABIDKA = 'frontend_product_all_categories';
    const CATEGORY = 'frontend_product_category';
    const PRODUCT = 'frontend_product_detail';

    /**
     * DashboardController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("all", name="list")
     * @param RouterInterface $router
     * @return \string[][]
     */
    public function list(RouterInterface $router)
    {
        return [
            [
                'name' => 'Stránka',
                'route' => self::PAGE,
            ],
            [
                'name' => 'Nabídka',
                'route' => self::NABIDKA
            ],
            [
                'name' => 'Kategorie',
                'route' => self::CATEGORY
            ],
            [
                'name' => 'Produkty',
                'route' => self::PRODUCT
            ],
        ];
    }
}
