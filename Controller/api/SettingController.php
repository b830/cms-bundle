<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\CmsBundle\Entity\Setting;
use App\Akip\CmsBundle\Repository\SettingRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class SettingController
 * @package App\Akip\CmsBundle\Controller\api
 * @Rest\Route("/api/setting", name="api_setting_")
 */
class SettingController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MenuController constructor.
     * @param EntityManagerInterface $em
     *
     *
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("", name="setting_list")
     * @param Request $request
     * @param SettingRepository $settingRepository
     * @param ParamFetcherInterface $pf
     * @return Setting[]
     * @Rest\View(serializerGroups={"list"})
     */
    public function list(Request $request, SettingRepository $settingRepository, ParamFetcherInterface $pf)
    {
        $group = $request->query->get('group');
        $settings = $group ? $settingRepository->findBy(['group' => $group]) : $settingRepository->findBy([], ['group' => 'ASC']);

        return $settings;
    }

    /**
     * @Rest\Get("/{id}", name="get_setting_by_id")
     * @param Setting $setting
     * @return mixed
     * @Rest\View(serializerGroups={"list"})
     */
    public function getSetting(Setting $setting = null)
    {
        if (empty($setting)) {
            ErrorMessages::message(ErrorMessages::SETTING_NOT_FOUND);
        }
        return $setting;
    }


    /**
     * @Rest\Put("", name="update_setting")
     * @Rest\View(serializerGroups={"list"})
     *
     * @param Setting $setting
     * @param Request $request
     * @param ValidatorInterface $validator
     */
    public function update(Request $request, ValidatorInterface $validator, SettingRepository $repository)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        foreach ($data as $item) {
            $setting = $repository->find($item['id']);
            if (!$setting)
                ErrorMessages::message(ErrorMessages::SETTING_NOT_FOUND);
            $setting->load($item);
        }
        $this->em->flush();
        if ($setting)
            return $repository->findBy(['group' => $setting->getGroup()]);
        else
            return  [];
    }
}
