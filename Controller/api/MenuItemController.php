<?php

namespace App\Akip\CmsBundle\Controller\api;


use App\Akip\CmsBundle\Entity\Menu;
use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Repository\MenuItemRepository;
use App\Akip\CmsBundle\Repository\MenuRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MenuItemController extends BaseController
{
    /**
     * @Rest\Delete("/api/menu/submenu/{id}", name="delete_submenu_by_id")
     * @Rest\View(serializerGroups={"deleteSubmenu"})
     * @param MenuItem $menuItem
     */
    public function deleteSubmenu(MenuItemRepository $menuItemRepository, MenuItem $menuItem)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($menuItem);
        $em->flush();
    }

    /**
     * @Rest\Get("/api/menu/submenu/all", name="menu_item_list")
     * @Rest\View(serializerGroups={"list"})
     */
    public function list(MenuItemRepository $repository) {
        return $repository->findAll();
    }
}
