<?php

namespace App\Akip\CmsBundle\Controller\api;


use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Entity\MenuItemTranslation;
use App\Akip\CmsBundle\Menu\MenuBuilder;
use App\Akip\CmsBundle\Repository\MenuItemRepository;
use App\Akip\CmsBundle\Repository\MenuItemTranslationRepository;
use App\Akip\CmsBundle\Repository\MenuRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use phpDocumentor\Reflection\Types\Parent_;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\Console\CommandLoader\FactoryCommandLoader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Akip\CmsBundle\Entity\Menu;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Constraints\NotNull;
use Knp\Menu\FactoryInterface;
use Knp\Bundle\MenuBundle\Provider;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\iterator;

class MenuController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MenuController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/api/menu", name="menu_list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default=10)
     * @Rest\QueryParam(name="offset", default=0)
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     * @param MenuRepository $menuRepository
     * @param MenuItemRepository $menuItemRepository
     * @param MenuItemTranslationRepository $menuItemTranslationRepository
     * @param ParamFetcherInterface $pf
     * @return mixed
     */
    public function list(MenuRepository $menuRepository, MenuItemRepository $menuItemRepository, MenuItemTranslationRepository $menuItemTranslationRepository, ParamFetcherInterface $pf)
    {
        $total = count($menuRepository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($menuRepository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('enabled')) {
            $criteria->andWhere(Criteria::expr()->eq('enabled', $this->getFilter('enabled')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $menuRepository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $menuRepository->matching($criteria);

        $filteredCount = count($data);
        $result = array();
        /**
         * @var $menu Menu
         */
        foreach ($data as $menu) {
            $result[] = [
                'id' => $menu->getId(),
                'slug' => $menu->getSlug(),
                'name' => $menu->getName(),
                'menuItems' => $menuRepository->build($menu, $menu->getItems(), $menuItemRepository, $menuItemTranslationRepository),
                'createdAt' => $menu->getCreatedAt()
            ];
        }
        return $this->listResponse($result, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @Rest\Get("/api/menu/{id}", name="get_menu_by_id")
     * @Rest\View(serializerGroups={"getMenu"})
     * @param Menu $menu
     */
    public function getMenu(MenuRepository $menuRepository, MenuItemRepository $menuItemRepository, MenuItemTranslationRepository $menuItemTranslationRepository, Menu $menu = null)
    {
        if (!$menu)
            ErrorMessages::message(ErrorMessages::MENU_NOT_FOUND);
        $result = [
            'id' => $menu->getId(),
            'slug' => $menu->getSlug(),
            'name' => $menu->getName(),
            'menuItems' => $menuRepository->build($menu, $menu->getItems(), $menuItemRepository, $menuItemTranslationRepository),
            'createdAt' => $menu->getCreatedAt()
        ];
        return $result;
    }

    /**
     * @Rest\Post("/api/menu", name="create_new_menu")
     * @Rest\View(serializerGroups={"getMenu"})
     * @param Request $request
     * @param MenuRepository $menuRepository
     * @param MenuItemRepository $menuItemRepository
     * @param MenuItemTranslationRepository $menuItemTranslationRepository
     * @param ValidatorInterface $validator
     * @return array
     */
    public function save(Request $request, MenuRepository $menuRepository, MenuItemRepository $menuItemRepository, MenuItemTranslationRepository $menuItemTranslationRepository, ValidatorInterface $validator)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $testSlug = $this->em->getRepository(Menu::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug){
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }
        $menu = new Menu();
        $menu = $menu->load($data);
//        if (BaseController::validate($menu, $validator))
//            return BaseController::validate($menu, $validator);
        $this->em->persist($menu);
        $this->em->flush();

        if (isset($data['menuItems'])) {
            $this->read($menu, $data['menuItems'], $validator);
        }

        /**
         * @var $mr MenuItemRepository
         */
        $mr = $this->em->getRepository(MenuItem::class);
        $mr->rebuildTree($menu->getId());
        return $this->getMenu($menuRepository, $menuItemRepository, $menuItemTranslationRepository, $menu);
    }

    /**
     * @Rest\Put("/api/menu/{id}", name="update_menu")
     * @Rest\View(serializerGroups={"getMenu"})
     * @param MenuRepository $menuRepository
     * @param MenuItemRepository $menuItemRepository
     * @param MenuItemTranslationRepository $menuItemTranslationRepository
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param Menu $oldMenu
     * @return array|JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update(MenuRepository $menuRepository, MenuItemRepository $menuItemRepository, MenuItemTranslationRepository $menuItemTranslationRepository, Request $request, ValidatorInterface $validator, Menu $oldMenu = null)
    {
        if (!$oldMenu)
            ErrorMessages::message(ErrorMessages::MENU_NOT_FOUND);

        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }

        $newMenu = $oldMenu->load($data);
        $testSlug = $this->em->getRepository(Menu::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug && $testSlug !== $newMenu){
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }
        if (BaseController::validate($newMenu, $validator))
            return BaseController::validate($newMenu, $validator);
        $this->em->flush();

        foreach ($newMenu->getMenuItems() as $menuItem) {
            foreach ($menuItem->getMenuItemTranslations() as $menuItemTranslation) {
                $menuItem->removeMenuItemTranslation($menuItemTranslation);
//                $this->em->remove($menuItemTranslation);
                $this->em->flush();
            }
            $newMenu->removeMenuItem($menuItem);
        }


        if (isset($data['menuItems'])) {
            $d = $this->read($newMenu, $data['menuItems'], $validator);
        }

        foreach ($d as $item) {
            /**
             * @var MenuItem $item
             */
            $newMenu->addMenuItem($item);
            $this->em->persist($item);
            foreach ($item->getMenuItemTranslations() as $translation) {
                $this->em->persist($translation);
//                    $this->em->flush();
            }
            $this->em->flush();
        }

        if (!$newMenu->getMenuItems()->isEmpty()) {
            /**
             * @var $mr MenuItemRepository
             */
            $mr = $this->em->getRepository(MenuItem::class);
            $mr->rebuildTree($newMenu->getId());
        }
        return $this->getMenu($menuRepository, $menuItemRepository, $menuItemTranslationRepository, $newMenu);
    }

    private function read(Menu $menu, $data, ValidatorInterface $validator, $parent = null)
    {
        $menus = array();
        foreach ($data as $item) {
            $mi = new MenuItem();
            $mi = $mi->load($item, $menu, $parent);
            if (BaseController::validate($mi, $validator)) {
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, BaseController::validate($mi, $validator));
            }
            if (isset($data['translations'])) {
                $this->checkLocale(array_keys($item['translations']));
            }
            if (isset($item['translations'])) {
                $save = true;
                foreach ($item['translations'] as $key => $translation) {
                    $mir = new MenuItemTranslation();
                    $mir = $mir->load($key, $translation, $mi);

                    $testSlug = $this->em->getRepository(MenuItemTranslation::class)->findOneBy(['slug' => $translation['slug']]);
                    if ($testSlug){
                        ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
                    }

                    if ($save === true){
                        $menu->addMenuItem($mi);
                        $this->em->persist($mi);
                        $this->em->flush();
                    }
                    $mi->addMenuItemTranslation($mir);
                    $this->em->persist($mir);
                    $this->em->flush();
//                    $save=false;
                }
            }
            array_push($menus, $mi);
//            $menu->addMenuItem($mi);
            if (!empty($item['children'])) {
                $this->read($menu, $item['children'], $validator, $mi);
            }
        }
        return $menus;
    }


    /**
     * @Rest\Delete("/api/menu/{id}", name="delete_menu_by_id")
     * @Rest\View(StatusCode = 204)
     * @param Menu $menu
     */
    public function deleteMenu(Menu $menu = null)
    {
        if (!$menu)
            ErrorMessages::message(ErrorMessages::MENU_NOT_FOUND);
        $this->em->remove($menu);
        $this->em->flush();
    }
}
