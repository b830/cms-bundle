<?php

namespace App\Akip\CmsBundle\Controller\api;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use phpDocumentor\Reflection\Element;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseController extends AbstractFOSRestController
{
    const JSON_DATE_FORMAT = 'Y-m-d\TH:i:s.v\Z';

    public function __construct()
    {
    }

    protected $filter = [];
    protected $order = [];

    public function checkLocale($locales)
    {
        $existLocales = explode("|", $_ENV['LOCALES']);
        foreach ($locales as $locale) {
            if (!in_array($locale, $existLocales)) {
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Locale [{$locale}] is not allowed. Allowed locales are - [{$_ENV['LOCALES']}]");
            }
        }
    }

    public function getLocalesList()
    {
        return $existLocales = explode("|", $_ENV['LOCALES']);
    }

    protected function loadTranslationsFromArray($data, $translation, $validator)
    {
        $translationArray = array();
        if (isset($data['translations'])) {
            foreach ($data as $key => $item) {
                $translation->load($key, $item);
                $valid = BaseController::validate($translation, $validator);
                $translationArray = $translation;
            }
        }
        return $translationArray;
    }

    static function validate($entity, ValidatorInterface $validator)
    {
        $errors = $validator->validate($entity);
        if (count($errors)) {

            $errorMessages = [];
            /**
             * @var $e ConstraintViolation
             */
            foreach ($errors as $e) {
                $errorMessages[$e->getPropertyPath()][] = $e->getMessage();
            }
            /**
             * @var $e ConstraintViolation
             */
            throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, new JsonResponse($errorMessages));
//            return new JsonResponse($errorMessages, 422);

            return (new JsonResponse($errorMessages, 422));
        }
    }

    private function errorMessage($message ,$prefix = '', $suffix = ''){
        return [
            'code' => $message['code'],
            'message' => $prefix . $message['message'] . $suffix,
            'status' => $message['status']
        ];
    }

    public function errorResponse($message){
        $response = new Response();
        $content = $this->errorMessage($message);
        $response->setStatusCode($message['status']);
        $response->setContent(json_encode($content));
        return $response;
    }

    protected function listResponse($data, $totalResultsCount, $filteredResultsCount, $limit, $offset)
    {
        if (count($data) <= 0)
            $data = array();
        return [
            'meta' => [
                'totalCount' => $totalResultsCount,
                'filteredCount' => $filteredResultsCount,
                'limit' => $limit,
                'offset' => $offset,
            ],
            'results' => $data
        ];
    }


    protected function getOrderBy($clientOrder)
    {
        if (!$clientOrder || !is_array($clientOrder)) {
            return [];
        }

        $order = [];
        foreach ($clientOrder as $co) {
            $order[$co['column']] = strtoupper(isset($co['dir']) ? $co['dir'] : 'asc');
        }
        return $order;
    }

    protected function getFilter($name)
    {
//        return $this->filter;
//        return $this->filter[$name];
        if (isset($this->filter[$name]) && !empty($this->filter[$name])) {
            return $this->filter[$name];
        }

        return false;
    }
}
