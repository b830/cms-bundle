<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\CmsBundle\Entity\Slider;
use App\Akip\CmsBundle\Entity\SliderSlide;
use App\Akip\CmsBundle\Entity\SliderSlideTranslation;
use App\Akip\CmsBundle\Repository\SliderRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class SliderController
 * @package App\Akip\CmsBundle\Controller
 * @Rest\Route("/api/slider", name="slider_")
 */
class SliderController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MenuController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("", name="list")
     * @param SliderRepository $sliderRepository
     * @param ParamFetcherInterface $pf
     * @return Slider[]
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default=10)
     * @Rest\QueryParam(name="offset", default=0)
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     *
     */
    public function list(SliderRepository $sliderRepository, ParamFetcherInterface $pf)
    {
        $total = count($sliderRepository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($sliderRepository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('enabled')) {
            $criteria->andWhere(Criteria::expr()->eq('enabled', $this->getFilter('enabled')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $sliderRepository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $sliderRepository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @Rest\Get("/{id}", name="get")
     * @param Slider|null $slider
     * @return mixed
     * @Rest\View(serializerGroups={"detail", "list"})
     */
    public function getSlider(Slider $slider = null)
    {
        if (empty($slider)) {
            ErrorMessages::message(ErrorMessages::SLIDER_NOT_FOUND);
        }
        return $slider;
    }

    /**
     * @Rest\Post("", name="save")
     * @Rest\View(serializerGroups={"list", "detail"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return mixed|JsonResponse
     */
    public function save(Request $request, ValidatorInterface $validator)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $slider = new Slider();
        $slider->load($data);

        $testSlug = $this->em->getRepository(Slider::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug) {
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }

        $this->em->persist($slider);
        $this->em->flush();

        return $slider;
    }

    /**
     * @Rest\Put("/{id}", name="update")
     * @Rest\View(serializerGroups={"list", "detail"})
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param Slider $slider
     * @return mixed|JsonResponse
     */
    public function update(Request $request, ValidatorInterface $validator, Slider $slider)
    {
        if (!$slider)
            ErrorMessages::message(ErrorMessages::SLIDER_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $slider->load($data);

        $testSlug = $this->em->getRepository(Slider::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug && $slider != $testSlug) {
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }

        if (BaseController::validate($slider, $validator))
            return BaseController::validate($slider, $validator);

        $dbItems = $this->em->getRepository(SliderSlide::class)->findBy(['slider' => $slider]);
        $dbItemsId = [];
        foreach ($dbItems as $item) {
            $dbItemsId[$item->getId()] = $item->getId();
        }
        foreach ($data['slides'] as $slideData) {
            if (!isset($slideData['id'])) {
                $slide = new SliderSlide();
                $this->loadSlide($slider, $slide, $slideData);
//                $this->loadSlideTranslations($slide, $slideData['translations']);
            } else {
                /** @var SliderSlide $slide */
                $slide = $this->em->getRepository(SliderSlide::class)->find($slideData['id']);
                if (!$slide) {
                    ErrorMessages::message(ErrorMessages::SLIDER_SLIDE_NOT_FOUND);
                }
                if (isset($dbItemsId[$slide->getId()])) {
                    unset($dbItemsId[$slide->getId()]);
                }
                $this->loadSlide($slider, $slide, $slideData);
//                $this->loadSlideTranslations($slide, $slideData['translations']);
            }
        }
        $this->em->flush();
        if (count($dbItemsId) > 0) {
            foreach ($dbItemsId as $item) {
                $slide = $this->em->getRepository(SliderSlide::class)->find($item);
                foreach ($slide->getTranslationsObj() as $translation) {
                    $this->em->remove($translation);
                }
                $this->em->remove($slide);
            }
            $this->em->flush();
        }
        return $slider;
    }

    private function loadSlide(Slider $slider, SliderSlide &$slide, $data)
    {
//        /** @var File $file */
//        $file = $this->em->getRepository(File::class)->findOneBy(['uuid' => $data['uuid']]);
//        if (!$file) {
//            ErrorMessages::message(ErrorMessages::FILE_NOT_FOUND);
//        }
//        $slide->load($data, $file, $slider);
        $slide->setSort($data['sort']);
        $slide->setEnabled($data['enabled']);
        $this->em->persist($slide);
    }

    private function loadSlideTranslations(SliderSlide $slide, $data)
    {
        $translationsId = [];
        $this->checkLocale(array_keys($data));
        if ($slide->getTranslationsObj()->count() > 0) {
            foreach ($slide->getTranslationsObj() as $translation) {
                $translationsId[$translation->getId()] = $translation->getId();
            }
        }
        foreach ($data as $locale => $translationData) {
            if (isset($translationData['id'])) {
                /** @var SliderSlideTranslation $translation */
                $translation = $this->em->getRepository(SliderSlideTranslation::class)->find($translationData['id']);
                if (!$translation) {
                    ErrorMessages::message(ErrorMessages::SLIDER_SLIDE_TRANSLATION_NOT_FOUND);
                }
                if (isset($translationsId[$translation->getId()])) {
                    unset($translationsId[$translation->getId()]);
                }
                $translation->load($translationData, $locale, $slide);
                $this->em->flush();
            } else {
                $translation = new SliderSlideTranslation();
                $translation->load($translationData, $locale, $slide);
                $slide->addTranslation($translation);
                $this->em->persist($translation);
            }
        }
        $this->em->flush();
        if (count($translationsId) > 0) {
            foreach ($translationsId as $item) {
                $sliderTranslation = $this->em->getRepository(SliderSlideTranslation::class)->find($item);
                $this->em->remove($sliderTranslation);
            }
            $this->em->flush();
        }
    }


    /**
     * @Rest\Delete("/{id}", name="delete")
     * @param Slider|null $slider
     * @Rest\View(StatusCode = 204)
     */
    public function delete(Slider $slider = null)
    {
        if (!$slider)
            ErrorMessages::message(ErrorMessages::SLIDER_SLIDE_NOT_FOUND);
        $this->em->remove($slider);
        $this->em->flush();
    }
}
