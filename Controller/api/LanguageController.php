<?php

namespace App\Akip\CmsBundle\Controller\api;


use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class LanguageController extends BaseController
{
    /**
     * @Rest\Get("api/language", name="language_list")
     */
    public function list()
    {
        $lan = $_ENV['LOCALES'];
        $lan_arr = explode("|", $lan);
        $def = $_ENV['DEFAULT_LOCALE'];
        foreach ($lan_arr as $item){
            if ($item === $def)
                $default = true;
            else
                $default = false;
            $data[] = [
                'code' => $item,
                'default' => $default
            ];
        }
        return $this->json($data);
    }
}
