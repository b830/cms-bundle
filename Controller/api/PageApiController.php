<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\CmsBundle\Entity\Menu;
use App\Akip\CmsBundle\Entity\Page;
use App\Akip\CmsBundle\Entity\PageTranslation;
use App\Akip\CmsBundle\Repository\PageRepository;
use Composer\DependencyResolver\Request;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Request\ParamFetcherInterface;
use mysql_xdevapi\Result;
use phpDocumentor\Reflection\DocBlock\Serializer;
use phpDocumentor\Reflection\Type;
use phpDocumentor\Reflection\Types\AbstractList;
use phpDocumentor\Reflection\Types\Collection;
use phpDocumentor\Reflection\Types\This;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PageApiController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * PageApiController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/api/page", name="page_list")
     * @Rest\View(serializerGroups={"list"})
     * @param PageRepository $pageRepository
     * @param ParamFetcherInterface $pf
     * @return Page[]
     * @Rest\QueryParam(name="limit", default=10)
     * @Rest\QueryParam(name="offset", default=0)
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     */
    public function list(PageRepository $pageRepository, ParamFetcherInterface $pf)
    {
        $total = count($pageRepository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($pageRepository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('enabled')) {
            $criteria->andWhere(Criteria::expr()->eq('enabled', $this->getFilter('enabled')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $pageRepository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $pageRepository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @Rest\Get("/api/page/all", name="get_page_all")
     * @Rest\View(serializerGroups={"list"})
     * @param PageRepository $repository
     * @return Page[]
     */
    public function getAll(PageRepository $repository)
    {
        return $repository->findAll();
    }

    /**
     * @Rest\Get("/api/page/{id}", name="get_page_by_id")
     * @Rest\View(serializerGroups={"detail"})
     * @param Page $page
     */
    public function getPage(Page $page = null)
    {
        if (empty($page)) {
            ErrorMessages::message(ErrorMessages::PAGE_NOT_FOUND);
        }
        return $page;
//        return $page->buildData();
    }

    /**
     * @Rest\Delete("/api/page/{id}", name="delete_page_by_id")
     * @Rest\View(StatusCode = 204)
     * @param Page $page
     */
    public function deletePage(Page $page = null)
    {
        if (!$page)
            ErrorMessages::message(ErrorMessages::PAGE_NOT_FOUND);
        $em = $this->getDoctrine()->getManager();
        $em->remove($page);
        $em->flush();
    }

    /**
     * @Rest\Post("/api/page", name="create_new_page")
     * @Rest\View(serializerGroups={"detail"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param ValidatorInterface $validator
     */
    public function save(\Symfony\Component\HttpFoundation\Request $request, ValidatorInterface $validator)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $p = new Page();
        $p = $p->load($data);

//        return $this->errorResponse(ErrorMessages::SLUG_EXISTS);
//        if (BaseController::validate($p, $validator) !== null)
//            return BaseController::validate($p, $validator);
//        var_dump($v);
        $this->checkLocale(array_keys($data['translations']));


        if (isset($data['translations'])) {
            $save = true;
            foreach ($data['translations'] as $key => $item) {
                $pr = new PageTranslation();
                $pr->load($item, $key, $p);
                if (BaseController::validate($pr, $validator) !== null)
                    return BaseController::validate($pr, $validator);
                if ($save === true) {
                    $this->em->persist($p);
                    $this->em->flush();
                }
                $p->addTranslation($pr);
                $this->em->persist($pr);
                $this->em->flush();
                $save = false;
            }
        }
        return $p;
    }


    /**
     * @Rest\Put("/api/page/{id}", name="update_page")
     * @Rest\View(serializerGroups={"detail"})
     * @param Page $page
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param ValidatorInterface $validator
     * @param PageRepository $pageRepository
     * @return Page
     */
    public function update(\Symfony\Component\HttpFoundation\Request $request, ValidatorInterface $validator, PageRepository $pageRepository, Page $page = null)
    {
        if (!$page)
            ErrorMessages::message(ErrorMessages::PAGE_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $page = $page->load($data);

        if (BaseController::validate($page, $validator) !== null)
            return BaseController::validate($page, $validator);

        if (isset($data['translations'])) {
            foreach ($data['translations'] as $key => $item) {
                $translation = new PageTranslation();
                $translation->load($item, $key, $page);

                $translationsArray[] = $translation;
            }
        }

        $this->checkLocale(array_keys($data['translations']));

        foreach ($page->getTranslations() as $oldTran) {
            $page->removeTranslation($oldTran);
        }
        $this->em->persist($page);
        $this->em->flush();

        foreach ($translationsArray as $translation) {
            $valid = BaseController::validate($translation, $validator);
            if (!empty($valid))
                return $valid;
            $this->em->persist($translation);
            $page->addTranslation($translation);
            $this->em->flush();
        }
        $page->getTranslations();
        return $page;
    }
}
