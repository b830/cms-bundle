<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Controller\api\BaseController;
use App\Akip\EshopBundle\Entity\Order;
use App\Akip\EshopBundle\Repository\OrderRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class DashboardController
 * @package App\Akip\CmsBundle\Controller
 * @Rest\Route("/api/dashboard/", name="dashboard_")
 */
class DashboardController extends BaseController
{
    private $em;

    /**
     * DashboardController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("orders", name="orders_count")
     * @param OrderRepository $repository
     */
    public function orders()
    {
        $repository = $this->em->getRepository(Order::class);
        return ['count' => count($repository->findTodayDemans())];
    }
}
