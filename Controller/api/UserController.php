<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Controller\api\BaseController;
use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\CmsBundle\Entity\User;
use App\Akip\CmsBundle\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use http\Cookie;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
        $this->validator = $validator;
    }

    /**
     * @Rest\Get("/api/user", name="user_list")
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default=10)
     * @Rest\QueryParam(name="offset", default=0)
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     * @param UserRepository $userRepository
     * @param ParamFetcherInterface $pf
     * @return array
     */
    public function list(UserRepository $userRepository, ParamFetcherInterface $pf)
    {
        $total = count($userRepository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($userRepository->getSearchCriteria($pf->get('search')));
        }

        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $userRepository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $userRepository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @Rest\Post("/api/user", name="registration")
     * @Rest\View(serializerGroups={"public"})
     * @param Request $request
     */
    public function save(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $user = new User();
        $user->load($data);
        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $data['password'])
        );
        $user->setRoles(['ROLE_ADMIN']);
        if (BaseController::validate($user, $this->validator)) {
            return BaseController::validate($user, $this->validator);
        }
        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }

    /**
     * @Rest\Put("/api/user/{id}", name="update_user")
     * @param User $user
     * @param Request $request
     * @Rest\View(serializerGroups={"public"})
     */
    public function update(Request $request, User $user = null)
    {
        $data = json_decode($request->getContent(), true);
        if (!$user)
            ErrorMessages::message(ErrorMessages::USER_NOT_FOUND);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $user->load($data);
        if (isset($data['password'])) {
            $user->setPassword(
                $this->passwordEncoder->encodePassword($user, $data['password'])
            );
        }
        if (isset($data['roles']))
            $user->setRoles(array_values($data['roles']));
        if (BaseController::validate($user, $this->validator))
            return BaseController::validate($user, $this->validator);
        $this->em->flush();
        return $user;
    }

    /**
     * @Rest\Get("/api/user/current", name="current_user_info")
     * @Rest\View(serializerGroups={"detail"})
     */
    public function current()
    {
        return $this->getUser();
    }

    /**
     * @Rest\Route("/api/user/logout", name="user_logout", methods={"GET"})
     */
    public function logout(Request $request)
    {
//        $cookie = new \Symfony\Component\HttpFoundation\Cookie('Bearer', 'xxxx', '1010-05-05', '/', null, null);
        $response = new Response();
        $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie('Bearer', 'xxxx', '1010-05-05', '/', null, null));
        $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie('RefreshToken', 'xxxx', '1010-05-05', '/', null, null));
//        $response->headers->setCookie(new \Symfony\Component\HttpFoundation\Cookie('PHPSESSID', 'xxxx', '1010-05-05', '/', null, null));
        return $response;
    }

    /**
     * @Rest\Get("/api/user/{id}", name="user_detail")
     * @Rest\View(serializerGroups={"detail"})
     * @param User|null $user
     * @return User
     */
    public function getUserEntity(User $user = null)
    {
        if (!$user)
            ErrorMessages::message(ErrorMessages::USER_NOT_FOUND);

        return $user;
    }


    /**
     * @Rest\Delete("/api/user/{id}", name="delete_user")
     * @Rest\View(StatusCode = 204)
     * @param User|null $user
     * @return void
     */
    public function delete(User $user = null)
    {
        if (!$user)
            ErrorMessages::message(ErrorMessages::USER_NOT_FOUND);
        if($user === $this->getUser())
            ErrorMessages::message(ErrorMessages::SELF_DELETE);

        $this->em->remove($user);
        $this->em->flush();
    }
}
