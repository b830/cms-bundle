<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\CmsBundle\Entity\Slider;
use App\Akip\CmsBundle\Entity\SliderSlide;
use App\Akip\CmsBundle\Entity\SliderSlideTranslation;
use App\Akip\CmsBundle\Repository\SliderRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class SliderController
 * @package App\Akip\CmsBundle\Controller
 * @Rest\Route("/api/slider", name="slider_slide")
 */
class SliderSlideController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MenuController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @Rest\Get("/slide/{id}", name="get")
     * @param SliderSlide|null $slide
     * @return mixed
     * @Rest\View(serializerGroups={"detail", "list"})
     */
    public function getSlider(SliderSlide $sliderSlide = null)
    {
        if (empty($sliderSlide)) {
            ErrorMessages::message(ErrorMessages::SLIDER_SLIDE_NOT_FOUND);
        }
        return $sliderSlide;
    }

    /**
     * @Rest\Post("/{id}/slide", name="save")
     * @Rest\View(serializerGroups={"list", "detail"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param Slider $slider
     * @return mixed|JsonResponse
     */
    public function save(Request $request, ValidatorInterface $validator, Slider $slider)
    {
        if (!$slider) {
            ErrorMessages::message(ErrorMessages::SLIDER_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }

        $slide = new SliderSlide();
        $this->loadSlide($slider, $slide, $data);
        $this->loadSlideTranslations($slide, $data['translations']);

        $this->em->persist($slider);
        $this->em->flush();

        return $slide;
    }

    /**
     * @Rest\Put("/slide/{id}", name="update")
     * @Rest\View(serializerGroups={"list", "detail"})
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SliderSlide $slide
     * @return mixed|JsonResponse
     */
    public function update(Request $request, ValidatorInterface $validator, SliderSlide $slide)
    {
        if (!$slide) {
            ErrorMessages::message(ErrorMessages::SLIDER_SLIDE_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $slider = $slide->getSlider();
        $this->loadSlide($slider, $slide, $data);
        $this->loadSlideTranslations($slide, $data['translations']);
        $this->em->flush();
        return $slide;
    }

    private function loadSlide(Slider $slider, SliderSlide &$slide, $data)
    {
        /** @var File $file */
        $file = $this->em->getRepository(File::class)->findOneBy(['uuid' => $data['uuid']]);
        if (!$file) {
            ErrorMessages::message(ErrorMessages::FILE_NOT_FOUND);
        }
        $slide->load($data, $file, $slider);
        $this->em->persist($slide);
    }

    private function loadSlideTranslations(SliderSlide $slide, $data)
    {
        $translationsId = [];
        $this->checkLocale(array_keys($data));
        if ($slide->getTranslationsObj()->count() > 0) {
            foreach ($slide->getTranslationsObj() as $translation) {
                $translationsId[$translation->getId()] = $translation->getId();
            }
        }
        foreach ($data as $locale => $translationData) {
            if (isset($translationData['id'])) {
                /** @var SliderSlideTranslation $translation */
                $translation = $this->em->getRepository(SliderSlideTranslation::class)->find($translationData['id']);
                if (!$translation) {
                    ErrorMessages::message(ErrorMessages::SLIDER_SLIDE_TRANSLATION_NOT_FOUND);
                }
                if (isset($translationsId[$translation->getId()])) {
                    unset($translationsId[$translation->getId()]);
                }
                $translation->load($translationData, $locale, $slide);
                $this->em->flush();
            } else {
                $translation = new SliderSlideTranslation();
                $translation->load($translationData, $locale, $slide);
                $slide->addTranslation($translation);
                $this->em->persist($translation);
            }
        }
        $this->em->flush();
        if (count($translationsId) > 0) {
            foreach ($translationsId as $item) {
                $sliderTranslation = $this->em->getRepository(SliderSlideTranslation::class)->find($item);
                $this->em->remove($sliderTranslation);
            }
            $this->em->flush();
        }
    }


    /**
     * @Rest\Delete("/slide/{id}", name="delete")
     * @param SliderSlide|null $sliderSlide
     * @Rest\View(StatusCode=204)
     */
    public function delete(SliderSlide $sliderSlide = null)
    {
        if (!$sliderSlide)
            ErrorMessages::message(ErrorMessages::SLIDER_SLIDE_NOT_FOUND);
        foreach ($sliderSlide->getTranslationsObj() as $translation) {
            $this->em->remove($translation);
        }
        $this->em->flush();
        $this->em->remove($sliderSlide);
        $this->em->flush();
    }
}
