<?php

namespace App\Akip\CmsBundle\Controller\api;

use App\Akip\CmsBundle\Entity\ErrorMessages;
use App\Akip\CmsBundle\Entity\PageTranslation;
use App\Akip\CmsBundle\Entity\Section;
use App\Akip\CmsBundle\Entity\SectionTranslation;
use App\Akip\CmsBundle\Repository\SectionRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SectionController extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * MenuController constructor.
     * @param EntityManagerInterface $em
     *
     *
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Rest\Get("/api/section", name="section_list")
     * @param SectionRepository $sectionRepository
     * @param ParamFetcherInterface $pf
     * @return Section[]
     * @Rest\View(serializerGroups={"list"})
     *
     * @Rest\QueryParam(name="limit", default=10)
     * @Rest\QueryParam(name="offset", default=0)
     *
     * @Rest\QueryParam(name="search")
     * @Rest\QueryParam(name="filter", map=true)
     * @Rest\QueryParam(name="order", map=true)
     *
     */
    public function list(SectionRepository $sectionRepository, ParamFetcherInterface $pf)
    {
        $total = count($sectionRepository->findAll());
        $this->filter = $pf->get('filter');
        $this->order = $this->getOrderBy($pf->get('order'));

        $criteria = Criteria::create();

        if ($pf->get('search')) {
            $criteria->andWhere($sectionRepository->getSearchCriteria($pf->get('search')));
        }
        // FILTERS
        if ($this->getFilter('enabled')) {
            $criteria->andWhere(Criteria::expr()->eq('enabled', $this->getFilter('enabled')));
        }
        // ORDER BY
        $criteria->orderBy($this->order);

        $filteredCount = $sectionRepository->matching($criteria)->count();

        // LIMIT, OFFSET
        $criteria->setFirstResult($pf->get('offset'));
        $criteria->setMaxResults($pf->get('limit'));

        $data = $sectionRepository->matching($criteria);

        $filteredCount = count($data);
        return $this->listResponse($data, $total, $filteredCount, $pf->get('offset'), $pf->get('limit'));
    }

    /**
     * @Rest\Get("/api/section/{id}", name="get_section_by_id")
     * @param Section $section
     * @return mixed
     * @Rest\View(serializerGroups={"detail", "list"})
     */
    public function getSection(Section $section = null)
    {
        if (empty($section)) {
            ErrorMessages::message(ErrorMessages::SECTION_NOT_FOUND);
        }
        return $section;
    }

    /**
     * @Rest\Post("/api/section", name="create_new_section")
     * @Rest\View(serializerGroups={"list", "detail"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return array|JsonResponse
     */
    public function save(Request $request, ValidatorInterface $validator)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $s = new Section();
        $s = $s->load($data);

        $testSlug = $this->em->getRepository(Section::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug){
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }

        $this->checkLocale(array_keys($data['translations']));

        if (isset($data['translations'])) {
            $save = true;
            foreach ($data['translations'] as $key => $item) {
                $sr = new SectionTranslation();
                $sr->load($item, $key, $s);
                if (BaseController::validate($sr, $validator) !== null)
                    return BaseController::validate($sr, $validator);
                if ($save === true) {
                    $this->em->persist($s);
                    $this->em->flush();
                }
                $s->addTranslation($sr);
                $this->em->persist($sr);
                $this->em->flush();
                $save = false;
            }
        }

        return $s;
    }

    /**
     * @Rest\Put("/api/section/{id}", name="update_section")
     * @Rest\View(serializerGroups={"list", "detail"})
     *
     * @param Section $oldSection
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return mixed|JsonResponse
     */
    public function update(Request $request, ValidatorInterface $validator, Section $section)
    {
        if (!$section)
            ErrorMessages::message(ErrorMessages::SECTION_NOT_FOUND);
        $data = json_decode($request->getContent(), true);
        if (empty($data) || !$data) {
            ErrorMessages::message(ErrorMessages::EMPTY_BODY);
        }
        $section = $section->load($data);

        $testSlug = $this->em->getRepository(Section::class)->findOneBy(['slug' => $data['slug']]);
        if ($testSlug && $section != $testSlug){
            ErrorMessages::message(ErrorMessages::SLUG_EXISTS);
        }

        if (BaseController::validate($section, $validator))
            return BaseController::validate($section, $validator);


        if (isset($data['translations'])) {
            foreach ($data['translations'] as $key => $item) {
                $translation = new SectionTranslation();
                $translation->load($item, $key, $section);

                $translationsArray[] = $translation;
            }
        }
        $this->checkLocale(array_keys($data['translations']));


        foreach ($section->getTranslations() as $oldTranslation) {
            $this->em->remove($oldTranslation);
            $this->em->flush();
        }
        $this->em->persist($section);
        $this->em->flush();

        foreach ($translationsArray as $translation) {
            $valid = BaseController::validate($translation, $validator);
            if (!empty($valid))
                return $valid;
            $this->em->persist($translation);
            $section->addTranslation($translation);
            $this->em->flush();
        }
        return $section;
    }

    /**
     * @Rest\Delete("/api/section/{id}", name="delete_section")
     * @param Section $section
     * @Rest\View(StatusCode = 204)
     */
    public function delete(Section $section = null)
    {
        if (!$section)
            ErrorMessages::message(ErrorMessages::SECTION_NOT_FOUND);
        $this->em->remove($section);
        $this->em->flush();
    }
}
