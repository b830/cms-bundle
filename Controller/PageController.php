<?php

namespace App\Akip\CmsBundle\Controller;

use App\Akip\CmsBundle\Entity\MenuItemTranslation;
use App\Akip\CmsBundle\Entity\Page;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PageController extends AbstractController
{

    /**
     * @Route("/{_locale}/{slug}",
     *     name="cms_page",
     *     requirements={"_locale":"%app.locales%", "slug":".+"})
     *
     * @Route("/{slug}",
     *     name="cms_page_default_locale",
     *     priority=-10,
     *     defaults={"_locale":"%app.default_locale%"},
     *     requirements={"slug":".+"})
     * @Rest\QueryParam(name="popup")
     * @param $slug
     * @param Request $request
     * @param ParamFetcherInterface $pf
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($slug, Request $request, ParamFetcherInterface $pf)
    {
        $em = $this->getDoctrine()->getManager();
        $popup = $pf->get('popup');

        /**
         * @var $menuItemTranslation MenuItemTranslation
         */
        $menuItemTranslation = $em->getRepository(MenuItemTranslation::class)->findOneBy([
            'slug' => $slug,
            'locale' => $request->getLocale()
        ]);

        if (!$menuItemTranslation) {
            $article = $em->getRepository(Page::class)->findArticle($slug, $request->getLocale());
            if (!$article) {
                throw $this->createNotFoundException('Article not found');
            }
        }
        if ($menuItemTranslation) {
            $menuItem = $menuItemTranslation->getMenuItem();

            $params = json_decode($menuItem->getRouteParameters());

            /**
             * @var $article Page
             */
            $article = $em->getRepository(Page::class)->findArticle($params->id, $request->getLocale());
        }
        $metadata = [];
        if ($article->getTranslationsObj()->first()){
            $metadata = json_decode($article->getTranslationsObj()->first()->getMetadata(), true);
        }

        if ($popup) {
            return $this->render('@AkipCms/article/popup.html.twig', [
                'controller_name' => 'ArticleController',
                'article' => $article,
                'menuItem' => $menuItemTranslation,
                'metadata' => $metadata,
                'locale' => $request->getLocale()
            ]);
        }
        return $this->render('@AkipCms/article/index.html.twig', [
            'controller_name' => 'ArticleController',
            'article' => $article,
            'menuItem' => $menuItemTranslation,
            'metadata' => $metadata,
            'locale' => $request->getLocale()
        ]);
    }
}
