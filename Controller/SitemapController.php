<?php

namespace App\Akip\CmsBundle\Controller;

use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Entity\MenuItemTranslation;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\EshopBundle\Entity\CategoryTranslation;
use App\Akip\EshopBundle\Entity\Product;
use App\Akip\EshopBundle\Entity\ProductVariant;
use App\Akip\EshopBundle\Entity\ProductVariantTranslation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SitemapController extends AbstractController
{

    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function showAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $urls = [];
        $hostname = $request->getSchemeAndHttpHost();

        $urls[] = ['loc' => $this->generateUrl('homepage_default_locale')];

        $menuItems = $em->getRepository(MenuItem::class)->findBy(['depth' => 1]);
        /** @var MenuItem $menuItem */
        foreach ($menuItems as $menuItem) {
            $menuItemsTranslations = $menuItem->getMenuItemTranslations();
            /** @var MenuItemTranslation $menuItemsTranslation */
            foreach ($menuItemsTranslations as $menuItemsTranslation) {
                if ($menuItemsTranslation->getSlug() === '/') {
                    continue;
                }
                $this->addToUrl(
                    $urls,
                    $menuItem->getRoute() === 'cms_page' ? 'cms_page_default_locale' : $menuItem->getRoute(),
                    ['slug' => $menuItem->getRoute() === 'cms_page' ? "{$menuItemsTranslation->getLocale()}/{$menuItemsTranslation->getSlug()}" : $menuItemsTranslation->getLocale()],
                    $menuItemsTranslation->getUpdatedAt()
                );
            }
        }

        $categories = $em->getRepository(Category::class)->findBy(['enabled' => true]);
        /** @var Category $category */
        foreach ($categories as $category) {
            $translations = $category->getTranslations();
            foreach ($translations as $translation) {
                $this->addToUrl($urls,'frontend_product_category', ['slug' => $translation->getSlug()], $translation->getUpdatedAt());
            }
        }

        $products = $em->getRepository(Product::class)->findBy(['enabled' => true]);
        /** @var ProductVariant $product */
        foreach ($products as $product) {
            $translations = $product->getTranslationsObj();
            foreach ($translations as $translation) {
                $this->addToUrl($urls, 'frontend_product_detail',['slug' => $translation->getSlug()], $translation->getUpdatedAt());
            }
        }

        // return response in XML format
        $response = new Response(
            $this->renderView('@AkipCms/sitemap/sitemap.html.twig', array('urls' => $urls,
                'hostname' => $hostname)),
            200
        );
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }

    private function addToUrl(&$urls, $route, $params, $lastMod, $priority = 1)
    {
        $urls[] = [
            'loc' => $this->generateUrl($route, $params),
            'lastmod' => $lastMod->format('Y-m-d'),
            'priority' => $priority
        ];
    }
}
