<?php


namespace App\Akip\CmsBundle\Listeners;



use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;

class RefreshTokenListener implements EventSubscriberInterface
{
    const REFRESH_TOKEN_COOKIE_NAME = 'RefreshToken';

    private $secure = false;
    private $tokenTtl;

    /**
     * RefreshTokenListener constructor.
     */
    public function __construct($tokenTtl)
    {
        $this->tokenTtl = $tokenTtl;
    }

    public function setRefreshToken(AuthenticationSuccessEvent $event)
    {
        $response = $event->getResponse();
        $data = $event->getData();

        $refreshToken = $data['refresh_token'];

        if ($refreshToken) {
            unset($data['refresh_token']);
            $event->setData($data);

            $cookieExpiration = new \DateTime();
            $cookieExpiration->add(new \DateInterval("PT{$this->tokenTtl}S"));

            $cookie = new Cookie(self::REFRESH_TOKEN_COOKIE_NAME, $refreshToken, $cookieExpiration, '/', null, $this->secure);

            $response->headers->setCookie($cookie);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'lexik_jwt_authentication.on_authentication_success' => [
                ['setRefreshToken']
            ]
        ];
    }


}