<?php

namespace App\Akip\CmsBundle\Listeners;


use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Cookie;

class AuthenticationSuccessListener
{
    const BEARER_COOKIE_NAME  = 'Bearer';

    private $secure = false;
    private $tokenTtl;

    /**
     * AuthenticationSuccessListener constructor.
     */
    public function __construct($tokenTtl)
    {
        $this->tokenTtl = $tokenTtl;
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
//        dump("onAuthenticationSuccess");

        $response = $event->getResponse();
        $data = $event->getData();

        $token = $data['token'];

        unset($data['token']);
        $event->setData($data);

        $cookieExpiration = new \DateTime();
        $cookieExpiration->add(new \DateInterval("PT{$this->tokenTtl}S"));

        $cookie = new Cookie(self::BEARER_COOKIE_NAME, $token, $cookieExpiration, '/', null, $this->secure);

        $response->headers->setCookie($cookie);
    }

}