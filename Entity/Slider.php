<?php

namespace App\Akip\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Akip\CmsBundle\Repository\SliderRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Section
 *
 * @ORM\Table(name="slider", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})})
 * @ORM\Entity(repositoryClass=SliderRepository::class)
 * @UniqueEntity("slug")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class Slider
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Gedmo\Versioned()
     * @Groups({"list", "detail"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $enabled;

    /**
     * @ORM\OneToMany(targetEntity=SliderSlide::class, mappedBy="slider",  orphanRemoval=true))
     * @Groups({"list", "detail"})
     * @ORM\OrderBy({"sort" = "ASC"})
    */
    private $slides;

    public function __construct()
    {
        $this->slides = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }

        $this->slug = $slug;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return Collection|SliderSlide[]
     */
    public function getSlides(): Collection
    {
        return $this->slides;
    }

    public function addSlide(SliderSlide $slide): self
    {
        if (!$this->slides->contains($slide)) {
            $this->slides[] = $slide;
            $slide->setSlider($this);
        }

        return $this;
    }

    public function removeSlide(SliderSlide $slide): self
    {
        if ($this->slides->contains($slide)) {
            $this->slides->removeElement($slide);
            // set the owning side to null (unless already changed)
            if ($slide->getSlider() === $this) {
                $slide->setSlider(null);
            }
        }
        return $this;
    }

    public function load($data)
    {
        $enabled = true;
        if (isset($data['enabled']))
            $enabled = $data['enabled'];
        elseif ($this->getEnabled())
            $enabled = $this->getEnabled();

        $this->setName($data['name']);
        $this->setSlug($data['slug']);
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return \DateTime
     * @Groups({"list"})
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
