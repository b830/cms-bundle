<?php

namespace App\Akip\CmsBundle\Entity;

use App\Akip\CmsBundle\Repository\SliderSlideTranslationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SliderSlideTranslation
 *
 * @ORM\Table(name="slider_slide_translation", indexes={@ORM\Index(name="FK_slide", columns={"slide_id"})})
 * @ORM\Entity(repositoryClass=SliderSlideTranslationRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class SliderSlideTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=true)
     * @Groups({"detail"})
     * @Gedmo\Versioned()
     */
    private $content = '';

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=10, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @var SliderSlide
     *
     * @ORM\ManyToOne(targetEntity="SliderSlide")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slide_id", referencedColumnName="id")
     * })
     */
    private $slide;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getSlide(): ?SliderSlide
    {
        return $this->slide;
    }

    public function setSlide(?SliderSlide $sliderSlide): self
    {
        $this->slide = $sliderSlide;

        return $this;
    }

    public function load($data, $locale, SliderSlide $sliderSlide)
    {
        $this->setLocale($locale);
        $this->setContent($data['content']);
        $this->setSlide($sliderSlide);
        return $this;
    }

}
