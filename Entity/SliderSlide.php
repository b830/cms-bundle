<?php

namespace App\Akip\CmsBundle\Entity;

use App\Akip\CmsBundle\Repository\SliderSlideRepository;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * sliderTranslation
 *
 * @ORM\Table(name="slider_slide", indexes={@ORM\Index(name="FK_slider", columns={"slider_id"})})
 * @ORM\Entity(repositoryClass=SliderSlideRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class SliderSlide
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"detail", "list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Gedmo\Versioned()
     * @Groups({"list", "detail"})
     */
    private $name = '';

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Groups({"list", "detail", "allList"})
     * @Gedmo\Versioned()
     */
    private $enabled = true;
    
    /**
     * @var int
     * @Groups({"list", "detail"})
     * @ORM\Column(name="sort", type="integer", nullable=false)
     * @Gedmo\Versioned()
     */
    private $sort = 99999;

    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="App\Akip\FileManagerBundle\Entity\File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="file_uuid", referencedColumnName="uuid", nullable=false)
     * })
     * @Groups({"detail"})
     */
    private $picture;

    /**
     * @var Slider
     *
     * @ORM\ManyToOne(targetEntity="Slider")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="slider_id", referencedColumnName="id")
     * })
     * @Gedmo\Versioned()
     */
    private $slider;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\CmsBundle\Entity\SliderSlideTranslation", mappedBy="slide", orphanRemoval=true)
     */
    private $translations;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $route = '';

    /**
     * @ORM\Column(type="text")
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $route_parameters = '{}';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getPicture(): ?File
    {
        return $this->picture;
    }

    public function setPicture(?File $file): self
    {
        $this->picture = $file;

        return $this;
    }

    public function getSlider(): ?Slider
    {
        return $this->slider;
    }

    public function setSlider(?Slider $slider): self
    {
        $this->slider = $slider;

        return $this;
    }

    /**
     * @return Collection|SliderSlideTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    //data for ProductController

    /**
     * @return array
     * @Groups({"detail"})
     */
    public function getTranslations()
    {
        $items = array();
        foreach ($this->translations as $translation) {
            /**
             * @var $translation SliderSlideTranslation
             */
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    public function addTranslation(SliderSlideTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setSlide($this);
        }

        return $this;
    }

    public function removeTranslation(SliderSlideTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getSlide() === $this) {
                $translation->setSlide(null);
            }
        }

        return $this;
    }

    public function load($data, File $file, Slider $slider)
    {
        $this->setName($data['name']);
        if (isset($data['sort'])) {
            $this->setSort($data['sort']);
        }
        if (isset($data['route'])) {
            $this->setRoute($data['route']);
        }
        if (isset($data['route_parameters'])) {
            if (is_array($data['route_parameters'])) {
                $this->setRouteParameters(json_encode($data['route_parameters']));
            }
        }
        $this->setEnabled($data['enabled']);
        $this->setSlider($slider);
        $this->setPicture($file);
        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getRouteParameters(): ?string
    {
        return $this->route_parameters;
    }

    public function setRouteParameters(string $route_parameters): self
    {
        $this->route_parameters = $route_parameters;

        return $this;
    }
}
