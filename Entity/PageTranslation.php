<?php

namespace App\Akip\CmsBundle\Entity;

use App\Akip\CmsBundle\Repository\PageTranslationRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PageTranslationRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class PageTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"detail", "create_new_page"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Page::class, inversedBy="translations")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $page;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"detail", "create_new_page"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"detail", "create_new_page"})
     * @Gedmo\Versioned()
     */
    private $introtext;

    /**
     * @ORM\Column(type="text")
     * @Groups({"detail", "create_new_page"})
     * @Gedmo\Versioned()
     */
    private $content;

    /**
     * @ORM\Column(type="text")
     * @Groups({"detail", "create_new_page"})
     * @Gedmo\Versioned()
     */
    private $metadata;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        if ($title === '' || !$title){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Title ');
        }
        $this->title = $title;

        return $this;
    }

    public function getIntrotext(): ?string
    {
        return $this->introtext;
    }

    public function setIntrotext(string $introtext): self
    {
        $this->introtext = $introtext;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getMetadata(): ?string
    {
        return $this->metadata;
    }

    public function setMetadata(string $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function load($data, $locale, Page $page)
    {
        $this->setLocale($locale);
        $this->setTitle($data['title']);
        $this->setIntrotext($data['introtext']);
        $this->setContent( $data['content']);
        $this->setMetadata( $data['metadata']);
        $this->setPage($page);
        return $this;
    }
}
