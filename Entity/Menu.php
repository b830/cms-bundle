<?php

namespace App\Akip\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Loggable\LoggableListener;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\DocBlock\Tags\Author;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Akip\CmsBundle\Repository\MenuRepository")
 * @UniqueEntity("slug")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class Menu
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     * Alias menu - lze pouzit pro vygenerovani
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     * Nazev menu
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\CmsBundle\Entity\MenuItem", mappedBy="menu", orphanRemoval=true)
     * @Groups({"list"})
     */
    private $menuItems;

    public function __construct()
    {
        $this->menuItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }
        $this->slug = $slug;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|MenuItem[]
     */
    public function getMenuItems(): Collection
    {
        return $this->menuItems;
    }

    /**
     * @return Collection|MenuItem[]
     */
    public function getItems(): Collection
    {
        return $this->menuItems;
    }

    public function getByDepth(int $depth)
    {
        $items = $this->menuItems;
        $data = array();
        foreach ($items as $item) {
            if ($item->getDepth() === $depth)
                array_push($data, $item);
        }
        return $data;
    }

    public function getItemsById(int $id)
    {
        $items = $this->menuItems;
        $data = array();
        foreach ($items as $item) {
            if ($item->getParent() && $item->getParent()->getId() === $id)
                array_push($data, $item);
        }
        return $data;
    }


    public function addMenuItem(MenuItem $menuItem): self
    {
        if (!$this->menuItems->contains($menuItem)) {
            $this->menuItems[] = $menuItem;
            $menuItem->setMenu($this);
        }

        return $this;
    }

    public function removeMenuItem(MenuItem $menuItem): self
    {
        if ($this->menuItems->contains($menuItem)) {
            $this->menuItems->removeElement($menuItem);
            // set the owning side to null (unless already changed)
            if ($menuItem->getMenu() === $this) {
                $menuItem->setMenu(null);
            }
        }

        return $this;
    }

    /**
     * @return \DateTime
     * @Groups({"detail", "getMenu"})
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function load($data)
    {
        $this->setSlug($data['slug']);
        $this->setName($data['name']);
        return $this;
    }

}
