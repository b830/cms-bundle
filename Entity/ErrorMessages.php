<?php


namespace App\Akip\CmsBundle\Entity;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ErrorMessages
{
    const SLUG_EXISTS = [
        'code' => 'unique_failed',
        'message' => "This slug already exists",
        'status' => '422'
    ];

    const EMPTY_BODY = [
        'code' => 'empty',
        'message' => "Empty body",
        'status' => '422'
    ];

    const SELF_DELETE = [
        'code' => 'self_delete',
        'message' => "Self delete is not allowed",
        'status' => '405'
    ];

    const NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Entity with specified id not found",
        'status' => '404'
    ];
    const PAGE_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Page with specified id not found",
        'status' => '404'
    ];
    const MENU_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Menu with specified id not found",
        'status' => '404'
    ];
    const SECTION_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Section with specified id not found",
        'status' => '404'
    ];
    const USER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "User with specified id not found",
        'status' => '404'
    ];
    const CATEGORY_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Category with specified id not found",
        'status' => '404'
    ];
    const CUSTOMER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Customer with specified id not found",
        'status' => '404'
    ];
    const COUNTRY_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Country with specified id not found",
        'status' => '404'
    ];
    const DEMAND_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Demand with specified id not found",
        'status' => '404'
    ];
    const PARAMETER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Parameter with specified id not found",
        'status' => '404'
    ];
    const COLOR_PARAMETER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Color parameter not found",
        'status' => '404'
    ];
    const SIZE_PARAMETER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Size parameter not found",
        'status' => '404'
    ];
    const PARAMETER_VALUE_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Parameter value with specified id not found",
        'status' => '404'
    ];
    const PARAMETER_TRANSLATION_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Parameter translation with specified id not found",
        'status' => '404'
    ];
    const PARAMETER_CANNOT_HAVE_VALUES = [
        'code' => 'failed',
        'message' => "This parameter cannot have values",
        'status' => '422'
    ];
    const PRICE_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Price with specified id not found",
        'status' => '422'
    ];
    const PRODUCT_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Product with specified id not found",
        'status' => '404'
    ];
    const PRODUCT_VARIANT_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Product variant with specified id not found",
        'status' => '404'
    ];
    const PHOTO_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Photo not found",
        'status' => '404'
    ];
    const FOLDER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Folder with specified id not found",
        'status' => '404'
    ];
    const FILE_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "File with specified uuid not found",
        'status' => '404'
    ];

    const CANNOT_BE_EMPTY = [
        'code' => 'cannot_be_empty',
        'message' => "cannot be empty",
        'status' => '422'
    ];

    const ID_COMBO_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Combination of ids not found",
        'status' => '404'
    ];

    const AUTHORIZATION_FAILED = [
        'code' => 'auth-0001',
        'message' => "Incorrect username or password",
        'status' => '403'
    ];

    const INVALID_TYPE = [
        'code' => 'validation_failed',
        'message' => "Invalid type",
        'status' => '422'
    ];
    const NOT_FOUND_IN_CART = [
        'code' => 'validation_failed',
        'message' => "Entity with specified it not found in cart",
        'status' => '422'
    ];
    const MAIN_CATEGORY_DELETE = [
        'code' => 'validation_failed',
        'message' => "Main category cannot be deleted",
        'status' => '422'
    ];
    const MAIN_VARIANT_DELETE = [
        'code' => 'validation_failed',
        'message' => "Main variant cannot be deleted",
        'status' => '422'
    ];
    const DEPLOYMENT = [
        'code' => 'validation_failed',
        'message' => "If you enter floor, you have to enable deployment",
        'status' => '422'
    ];
    const SETTING_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Setting not found",
        'status' => '404'
    ];
    const DEMAND_SEND_FAIL = [
        'code' => 'demand_send_fail',
        'message' => "Demand cannot be send",
        'status' => '400'
    ];
    const DEMAND_ALREADY_SEND = [
        'code' => 'demand_send_fail',
        'message' => "Demand has been already send",
        'status' => '404'
    ];
    const BRAND_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Brand with specified id not found",
        'status' => '404'
    ];
    const SLIDER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Slider with specified id not found",
        'status' => '404'
    ];
    const SLIDER_SLIDE_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Slider slide with specified id not found",
        'status' => '404'
    ];
    const SLIDER_SLIDE_TRANSLATION_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Slider slide translation with specified id not found",
        'status' => '404'
    ];
    const STOCK_STATUS_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Stock status with specified id not found",
        'status' => '404'
    ];
    const MENU_ITEM_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Menu item with specified id not found",
        'status' => '404'
    ];
    const PAYMENT_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Payment with specified id not found",
        'status' => '404'
    ];
    const DELIVERY_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Payment with specified id not found",
        'status' => '404'
    ];
    const COMBINATION_ALREADY_EXISTS = [
        'code' => 'is_not_empty',
        'message' => "Combination of this parameters already exists",
        'status' => '422'
    ];
    const ORDER_NOT_FOUND = [
        'code' => 'not_found',
        'message' => "Order with specified id not found",
        'status' => '404'
    ];
    const HEUREKA_LOAD_FAIL = [
        'code' => 'load_fail',
        'message' => "Occurs error when trying to load Heureka categories",
        'status' => '404'
    ];
    const ZBOZICZ_LOAD_FAIL = [
        'code' => 'load_fail',
        'message' => "Occurs error when trying to load Zbozi.cz categories",
        'status' => '404'
    ];
    const GOOGLE_MERCHANTS_LOAD_FAIL = [
        'code' => 'load_fail',
        'message' => "Occurs error when trying to load Google categories",
        'status' => '404'
    ];
    const ORDER_IS_PAYED = [
        'code' => 'was_payed',
        'message' => "Order was already payed",
        'status' => '404'
    ];
    const VALUE_CANNOT_BE_DELETED = [
        'code' => 'cannot_be_deleted',
        'message' => "Parameter value cannot be deleted",
        'status' => '404'
    ];

    static function message($errorMsg, $prefix = '', $suffix = '')
    {
        $errorMsg['message'] = $prefix . $errorMsg['message'] . $suffix;
        throw new HttpException($errorMsg['status'], $errorMsg['message']);
        die();
    }
}
