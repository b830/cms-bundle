<?php

namespace App\Akip\CmsBundle\Entity;

use App\Akip\CmsBundle\Repository\SettingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Section
 *
 * @ORM\Table(name="setting")
 * @ORM\Entity(repositoryClass=SettingRepository::class)
 * @UniqueEntity("slug")
 */
class Setting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups("list")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="group", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups("list")
     */
    private $group;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups("list")
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="value", type="text", length=0, nullable=true)
     * @Groups("list")
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Groups("list")
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_html", type="boolean", nullable=false)
     * @Groups("list")
     */
    private $isHtml = false;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroup(): ?string
    {
        return $this->group;
    }

    public function setGroup(string $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getIsHtml(): ?bool
    {
        return $this->isHtml;
    }

    public function setIsHtml(bool $isHtml): self
    {
        $this->isHtml = $isHtml;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function load($data)
    {
        $this->setValue($data['value']);
        $this->setIsHtml($data['isHtml']);
    }
}
