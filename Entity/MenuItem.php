<?php

namespace App\Akip\CmsBundle\Entity;

use App\Akip\CmsBundle\Repository\MenuRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Akip\CmsBundle\Repository\MenuItemRepository")
 * @ORM\Table(indexes={@ORM\Index(columns={"lft", "rgt"})})
 * @see https://php.vrana.cz/traverzovani-kolem-stromu-prakticky.php
 * @see https://www.interval.cz/clanky/metody-ukladani-stromovych-dat-v-relacnich-databazich/
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class MenuItem
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"list", "getMenu"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Akip\CmsBundle\Entity\Menu", inversedBy="menuItems")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotNull()
     * @Gedmo\Versioned()
     */
    private $menu;

    /**
     * @ORM\OneToMany(targetEntity="App\Akip\CmsBundle\Entity\MenuItemTranslation", mappedBy="menuItem", orphanRemoval=true)
     * @Groups({"list", "getMenu"})
     */
    private $menuItemTranslations;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list", "getMenu"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Akip\CmsBundle\Entity\MenuItem")
     * @Groups("getMenu")
     * @Gedmo\Versioned()
     */
    private $parent;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"list", "getMenu"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $sort = 0;

    /**
     * @ORM\Column(type="integer")
     * @see https://php.vrana.cz/traverzovani-kolem-stromu-prakticky.php
     * @see https://www.interval.cz/clanky/metody-ukladani-stromovych-dat-v-relacnich-databazich/
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $lft;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $rgt;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"list", "getMenu"})
     * @Gedmo\Versioned()
     */
    private $depth = 0;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $route;

    /**
     * @ORM\Column(type="text")
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $route_parameters;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list"})
     * @Gedmo\Versioned()
     */
    private $class = '';


    public function __construct()
    {
        $this->menuItemTranslations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    public function setMenu(?Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * @return Collection|MenuItemTranslation[]
     */
    public function getMenuItemTranslations(): Collection
    {
        return $this->menuItemTranslations;
    }

    public function getMenuItemTrans()
    {
        $arr = array();
        foreach ($this->menuItemTranslations as $itemTranslation) {
            array_push($arr, $itemTranslation->getLocale());
        }
        return $arr;
    }

    public function addMenuItemTranslation(MenuItemTranslation $menuItemTranslation): self
    {
        if (!$this->menuItemTranslations->contains($menuItemTranslation)) {
            $this->menuItemTranslations[] = $menuItemTranslation;
            $menuItemTranslation->setMenuItem($this);
        }

        return $this;
    }

    public function removeMenuItemTranslation(MenuItemTranslation $menuItemTranslation): self
    {
        if ($this->menuItemTranslations->contains($menuItemTranslation)) {
            $this->menuItemTranslations->removeElement($menuItemTranslation);
            // set the owning side to null (unless already changed)
            if ($menuItemTranslation->getMenuItem() === $this) {
                $menuItemTranslation->setMenuItem(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?MenuItem
    {
        return $this->parent;
    }

    public function setParent(?MenuItem $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort($sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getLft(): ?int
    {
        return $this->lft;
    }

    public function setLft(int $lft): self
    {
        $this->lft = $lft;

        return $this;
    }

    public function getRgt(): ?int
    {
        return $this->rgt;
    }

    public function setRgt(int $rgt): self
    {
        $this->rgt = $rgt;

        return $this;
    }

    public function getDepth(): ?int
    {
        return $this->depth;
    }

    public function setDepth(int $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(string $route): self
    {
        if ($route === '' || !$route){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Route ');
        }
        $this->route = $route;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getRouteParameters(): ?string
    {
        return $this->route_parameters;
    }

    public function setRouteParameters(string $route_parameters): self
    {
        if ($route_parameters === '' || !$route_parameters){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Route parameters ');
        }
        $this->route_parameters = $route_parameters;

        return $this;
    }

    public function load($data, Menu $menu, $parent)
    {
        $this->setName($data['name']);
        $this->setRoute($data['route']);
        if (is_array($data['route_parameters'])) {
            $data['route_parameters'] = json_encode($data['route_parameters']);
        }
        $this->setRouteParameters($data['route_parameters']);
        if (isset($data['class']))
            $this->setClass($data['class']);
        if (isset($data['sort']))
            $this->sort = $data['sort'];
        $this->lft = 0;
        $this->rgt = 0;
        if (isset($data['depth']))
            $this->depth = $data['depth'];
        $this->parent = $parent;
        $this->menu = $menu;
        return $this;
    }

}
