<?php

namespace App\Akip\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \App\Akip\CmsBundle\Entity\MenuItem;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="menu_item_translation", uniqueConstraints={@ORM\UniqueConstraint(name="slug", columns={"slug", "deleted_at"})})
 * @ORM\Entity(repositoryClass="App\Akip\CmsBundle\Repository\MenuItemTranslationRepository")
 * @UniqueEntity("slug")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class MenuItemTranslation
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Akip\CmsBundle\Entity\MenuItem", inversedBy="menuItemTranslations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $menuItem;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list", "getMenu"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list", "getMenu"})
     * * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"list"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $locale;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMenuItem(): ?MenuItem
    {
        return $this->menuItem;
    }

    public function setMenuItem(?MenuItem $menuItem): self
    {
        $this->menuItem = $menuItem;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        if ($name === '' || !$name){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Name ');
        }
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }
        $this->slug = $slug;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function load($locale ,$data, MenuItem $menuItem)
    {
        $this->setName($data['name']);
        $this->setSlug($data['slug']);
        $this->setLocale($locale);
        $this->menuItem = $menuItem;

        return $this;
    }
}
