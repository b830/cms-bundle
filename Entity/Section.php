<?php

namespace App\Akip\CmsBundle\Entity;

use App\Akip\CmsBundle\Repository\SectionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Section
 *
 * @ORM\Table(name="section")
 * @ORM\Entity(repositoryClass=SectionRepository::class)
 * @UniqueEntity("slug")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", hardDelete=false)
 * @Gedmo\Loggable()
 */
class Section
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"list", "detail"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Gedmo\Versioned()
     * @Groups({"list", "detail"})
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $slug;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     * @Groups({"list", "detail"})
     * @Gedmo\Versioned()
     */
    private $enabled;

    /**
     * @ORM\OneToMany(targetEntity=SectionTranslation::class, mappedBy="section",  orphanRemoval=true))
    */
    private $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        if ($slug === '' || !$slug){
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Slug ');
        }

        $this->slug = $slug;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @Groups({"detail"})
     */
    public function getTranslations(): array
    {
        $items = array();
        foreach ($this->translations as $translation) {
            /**
             * @var $translation SectionTranslation
             */
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    /**
     * @return Collection|SectionTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(SectionTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setSection($this);
        }

        return $this;
    }

    public function removeTranslation(SectionTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getSection() === $this) {
                $translation->setSection(null);
            }
        }
        return $this;
    }

    public function load($data)
    {
        $enabled = true;
        if (isset($data['enabled']))
            $enabled = $data['enabled'];
        elseif ($this->getEnabled())
            $enabled = $this->getEnabled();

        $this->setTitle($data['title']);
        $this->setSlug($data['slug']);
        $this->enabled = $enabled;
        return $this;
    }
    public function buildData()
    {
        $items = array();
        if (!$this->getTranslations()->isEmpty()) {
            foreach ($this->getTranslations() as $translation) {
                $items[$translation->getLocale()] = $translation;
            }
            $result = [
                'id' => $this->getId(),
                'title' => $this->getTitle(),
                'slug' => $this->getSlug(),
                'translations' => $items,
                'enabled' => $this->enabled,
                'createdAt' => $this->getCreatedAt()
            ];
        } else {
            $result = [
                'id' => $this->getId(),
                'title' => $this->getTitle(),
                'slug' => $this->getSlug(),
                'translations' => null,
                'enabled' => $this->enabled,
                'createdAt' => $this->getCreatedAt()
            ];
        }
        return $result;
    }

    /**
     * @return \DateTime
     * @Groups({"list", "getSection"})
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
