<?php

namespace App\Akip\CmsBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\Object_;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Akip\CmsBundle\Repository\PageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormTypeInterface;

/**
 * @ORM\Entity(repositoryClass=PageRepository::class)
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class Page
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"list", "detail", "create_new_page"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list", "detail", "create_new_page"})
     * @Assert\NotBlank()
     * @Gedmo\Versioned()
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"list", "detail", "create_new_page"})
     * @Gedmo\Versioned()
     */
    private $template;

    /**
     * @ORM\OneToMany(targetEntity=PageTranslation::class, mappedBy="page", orphanRemoval=true)
     * @Groups({"detail", "create_new_page"})
     */
    private $translations;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"detail", "list"})
     * @Gedmo\Versioned()
     */
    private $enabled = true;


    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title)
    {
        if ($title === '' || !$title) {
            ErrorMessages::message(ErrorMessages::CANNOT_BE_EMPTY, 'Title ');
        }
        $this->title = $title;

        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @Groups({"detail", "create_new_page"})
     */
    public function getTranslations(): array
    {
        $items = array();
        foreach ($this->translations as $translation) {
            /**
             * @var $translation PageTranslation
             */
            $items[$translation->getLocale()] = $translation;
        }
        return $items;
    }

    /**
     * @return Collection|PageTranslation[]
     */
    public function getTranslationsObj(): Collection
    {
        return $this->translations;
    }

    public function addTranslation(PageTranslation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setPage($this);
        }

        return $this;
    }

    public function removeTranslation(PageTranslation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getPage() === $this) {
                $translation->setPage(null);
            }
        }
        return $this;
    }

    /**
     * @return \DateTime
     * @Groups({"list", "detail"})
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function buildData()
    {
        $items = array();
        if ($this->getTranslations()[0] !== null) {
            foreach ($this->getTranslations() as $translation) {
                $items[$translation->getLocale()] = $translation;
            }

        }
        $result = [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'template' => $this->getTemplate(),
            'enabled' => $this->enabled,
            'translations' => $items
        ];
        return $result;
    }

    public function load($data)
    {
        $enabled = true;
        if (isset($data['enabled']))
            $enabled = $data['enabled'];
        elseif ($this->getEnabled())
            $enabled = $this->getEnabled();

        if (isset($data['template'])) {
            $this->setTemplate($data['template']);
        } else {
            $this->setTemplate('default');
        }
        

        $this->setTitle($data['title']);
        $this->enabled = $enabled;
        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

}
