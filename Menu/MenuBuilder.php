<?php


namespace App\Akip\CmsBundle\Menu;


use App\Akip\CmsBundle\Controller\api\RouteController;
use App\Akip\CmsBundle\Entity\Menu;
use App\Akip\CmsBundle\Entity\MenuItem;
use App\Akip\CmsBundle\Entity\MenuItemTranslation;
use App\Akip\CmsBundle\Repository\MenuItemRepository;
use App\Akip\CmsBundle\Repository\MenuItemTranslationRepository;
use App\Akip\EshopBundle\Entity\Category;
use App\Akip\FileManagerBundle\Entity\File;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityNotFoundException;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Translation\Translator;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var Translator
     */
    private $translator = null;

    private $defaultLocale = null;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     * @throws EntityNotFoundException
     *
     * @see MenuItemRepository::getMenuItems
     */
    public function main(FactoryInterface $factory, array $options)
    {

        $this->defaultLocale = $_ENV['DEFAULT_LOCALE'];

        /**
         * @var $em ManagerRegistry
         */
        $em = $this->container->get('doctrine')->getManager();

        /**
         * @var $t Translator
         */
        $this->translator = $this->container->get('translator');

        /**
         * @var $menuParent Menu
         */
        $menuParent = $em->getRepository(Menu::class)->findOneBy(['slug' => $options['menu']]);
        if (!$menuParent) {
            throw new EntityNotFoundException('Menu does not exist');
        }

        /**
         * @var $items MenuItem[]
         */
        $items = $em->getRepository(MenuItem::class)->getMenuItems($menuParent->getId(), $this->translator->getLocale());

        $menu = $factory->createItem('root');
        $itemsCache = [];

        if ($this->translator->getLocale() === $this->defaultLocale) {
            $menu->addChild('homepage', [
                'route' => 'homepage_default_locale',
            ]);
        } else {
            $menu->addChild('homepage', [
                'route' => 'homepage'
            ]);
        }

        foreach ($items as $item) {
            if ($item['p_id'] === null) {
                $itemsCache[$item['mi_id']] = $this->addMenuItem($menu, $item);
                continue;
            }

            $itemsCache[$item['mi_id']] = $this->addMenuItem($itemsCache[$item['p_id']], $item);
        }
        return $menu;
    }

    private function addMenuItem(ItemInterface $parent, array $item)
    {

//        $routeParams = $item['mi_route_parameters'] ? json_decode($item['mi_route_parameters'], true) : [];

        $routeParameters = [
            'slug' => $item['mit_slug'] ? $item['mit_slug'] : 'empty-slug'
        ];

        $route = $item['mi_route'];


        /**
         * @var $translator Translator
         */
//        $translator = $this->container->get('translator');

        if ($item['mit_locale'] === $this->defaultLocale) {
            $route .= '_default_locale';
        }

        $options = [
            'route' => $route,                        // dle routy symfony
//            'routeParameters' => array_merge($routeParams, [
            'routeParameters' => $routeParameters,
//            'display' => false,                           // zobrazit ano/ne
//            'displayChildren' => false,                   // zobrazit deti ano/ne
//            'attributes' => [],                           // atributy <li>
//            'linkAttributes' => ['target' => '_blank'],     // atributy <a>
            'label' => $item['mi_name']
        ];

        if ($item['mit_id']) {
            $options['label'] = $item['mit_name'];
        }

//        if ($item['mit_slug']) {
//            $options['uri'] = $item['mit_slug'];
//        }

        return $parent->addChild('item' . $item['mi_id'], $options);
    }

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return ItemInterface
     * @throws EntityNotFoundException
     *
     * @see MenuItemRepository::getMenuItems
     */
    public function header(FactoryInterface $factory, array $options)
    {
        if (!isset($options['mobile'])) {
            $options['mobile'] = false;
        }
        $this->defaultLocale = $_ENV['DEFAULT_LOCALE'];
        /**
         * @var $em ManagerRegistry
         */
        $em = $this->container->get('doctrine')->getManager();

        /**
         * @var $t Translator
         */
        $this->translator = $this->container->get('translator');

        /**
         * @var $menuParent Menu
         */
        $menuParent = $em->getRepository(Menu::class)->findOneBy(['slug' => $options['menu']]);
        if (!$menuParent) {
            throw new EntityNotFoundException('Menu does not exist');
        }

        /**
         * @var $items MenuItem[]
         */
        $items = $em->getRepository(MenuItem::class)->getMenuItems($menuParent->getId(), $this->translator->getLocale());

        $menu = $factory->createItem('root');
        $itemsCache = [];



        foreach ($items as $item) {
            if ($item['p_id'] === null) {
                $itemsCache[$item['mi_id']] = $this->addHeaderItem($menu, $item, $em);
                continue;
            }

            $itemsCache[$item['mi_id']] = $this->addHeaderItem($itemsCache[$item['p_id']], $item, $em);
        }

        return $menu;
    }

    private function addHeaderItem(ItemInterface $parent, array $item, $em)
    {
        $routeParameters = [
            'slug' => $item['mit_slug'] ? $item['mit_slug'] === '/' ? './' : $item['mit_slug'] : 'empty-slug'
        ];

        $route = $item['mi_route'];
        $icon = null;
        if ($route === RouteController::CATEGORY) {
            $routeParams = json_decode($item['mi_route_parameters'], true);
            if (isset($routeParams['id'])) {
                /** @var Category $category */
                $category = $em->getRepository(Category::class)->getByIdAndLocale($routeParams['id'], $this->translator->getLocale());
                if ($category !== null) {
                    $categoryTranslation = $category->getTranslations()->first();
                    if ($categoryTranslation) {
                        $routeParameters = ['slug' => $categoryTranslation->getSlug()];
                    }
                    if ($category && $category->getIcon() !== null) {
                        /** @var File $file */
                        $file = $category->getIcon();
                        if ($file) {
                            $file = $file->getChildren()->get(2);
                            $icon = $file->getUrl();
                        }
                    }
                } else {
                    $route = RouteController::PAGE;
                    $routeParameters = ['slug' => '404'];
                    $icon = 'https://image.shutterstock.com/image-vector/default-ui-image-placeholder-wireframes-260nw-1037719192.jpg';
                }
            }
        }
        if (!$icon && $route === RouteController::CATEGORY) {
            $icon = 'https://image.shutterstock.com/image-vector/default-ui-image-placeholder-wireframes-260nw-1037719192.jpg';
        }

        /**
         * @var $translator Translator
         */
//        $translator = $this->container->get('translator');

        if ($item['mit_locale'] === $this->defaultLocale && preg_match('/^cms_page/', $route)) {
            $route .= '_default_locale';
        } elseif ($route !== RouteController::CATEGORY) {
            $routeParameters = ['slug' => $item['mit_slug'] ? $item['mit_slug'] : 'empty-slug'];
        }
        $class = $item['mi_class'] ? $item['mi_class'] : '';
        $options = [
            'route' => $route,                        // dle routy symfony
//            'routeParameters' => array_merge($routeParams, [
            'routeParameters' => $routeParameters,
//            'display' => false,                           // zobrazit ano/ne
//            'displayChildren' => false,                   // zobrazit deti ano/ne
            'attributes' => ['class' => $class, 'icon' => $icon],                           // atributy <li>
//            'linkAttributes' => ['target' => '_blank'],     // atributy <a>
            'label' => $item['mi_name'],
            'icon' => $icon
        ];

        if ($item['mit_id']) {
            $options['label'] = $item['mit_name'];
        }

//        if ($item['mit_slug']) {
//            $options['uri'] = $item['mit_slug'];
//        }

        return $parent->addChild('item' . $item['mi_id'], $options);
    }

}
